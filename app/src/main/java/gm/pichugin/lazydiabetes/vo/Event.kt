package gm.pichugin.lazydiabetes.vo

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(primaryKeys = ["noteId", "eventType"])
data class Event(
    @PrimaryKey
    val noteId: Long,
    val eventType: EventType,
    val value: String,
    val unit: String
)