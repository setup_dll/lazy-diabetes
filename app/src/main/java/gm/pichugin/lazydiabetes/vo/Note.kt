package gm.pichugin.lazydiabetes.vo

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Note(
    @PrimaryKey
    val id: Long,
    val calendar: Calendar,
    val events: List<Event>
//    , val tags: List<String>
)