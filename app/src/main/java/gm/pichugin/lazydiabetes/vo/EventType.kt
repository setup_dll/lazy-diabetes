package gm.pichugin.lazydiabetes.vo

enum class EventType {
    GLUCOSE,
    BASAL,
    BOLUS,
    FOOD
}