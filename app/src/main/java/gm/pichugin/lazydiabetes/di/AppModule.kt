package gm.pichugin.lazydiabetes.di

import android.app.Application
import android.os.AsyncTask
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import gm.pichugin.lazydiabetes.db.Database
import gm.pichugin.lazydiabetes.db.NoteDao
import gm.pichugin.lazydiabetes.network.RestApi
import gm.pichugin.lazydiabetes.util.ColorUtils
import gm.pichugin.lazydiabetes.util.LiveDataCallAdapterFactory
import gm.pichugin.lazydiabetes.vo.Note
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module(includes = [ViewModelModule::class])
class AppModule {
    /**
     * Provides the REST service implementation.
     * @param retrofit the Retrofit object used to instantiate the service
     * @return the REST service implementation.
     */
    @Singleton
    @Provides
    fun provideApplicationApi(retrofit: Retrofit): RestApi {
        return retrofit.create(RestApi::class.java)
    }

    /**
     * Provides the OkHttpClient object.
     * @return the OkHttpClient object
     */
    @Singleton
    @Provides
    fun provideHttpClient(): OkHttpClient {
        return OkHttpClient.Builder().apply {
            this.addInterceptor(HttpLoggingInterceptor().apply {
                this.level = HttpLoggingInterceptor.Level.BASIC
            })
        }.build()
    }

    /**
     * Provides the Retrofit object.
     * @return the Retrofit object
     */
    @Singleton
    @Provides
    fun provideRetrofitInterface(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
//            .baseUrl("http://5cac2bc3c85e05001452efeb.mockapi.io")
            .baseUrl("https://glycemix.herokuapp.com")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(client)
            .build()
    }

    @Singleton
    @Provides
    fun provideGiUtils(application: Application): ColorUtils {
        return ColorUtils(application)
    }

    @Singleton
    @Provides
    fun provideDatabase(application: Application): Database {
        return Room
            .databaseBuilder(application, Database::class.java, "lazydi.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideNoteDao(database: Database): NoteDao {
        return database.noteDao()
    }
}