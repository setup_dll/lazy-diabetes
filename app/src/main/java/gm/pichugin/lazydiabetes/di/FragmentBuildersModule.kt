package gm.pichugin.lazydiabetes.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import gm.pichugin.lazydiabetes.ui.note.NoteListFragment

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeFoodListFragment(): NoteListFragment
}