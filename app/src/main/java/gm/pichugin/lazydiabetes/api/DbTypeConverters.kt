/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gm.pichugin.lazydiabetes.api

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import gm.pichugin.lazydiabetes.vo.Event
import java.util.*

object DbTypeConverters {
    @TypeConverter
    @JvmStatic
    fun toCalendar(l: Long?): Calendar? =
        if (l == null) null else Calendar.getInstance().apply { timeInMillis = l }

    @TypeConverter
    @JvmStatic
    fun fromCalendar(c: Calendar?): Long? = c?.time?.time

    @TypeConverter
    @JvmStatic
    fun fromNoteList(value: List<Event>): String {
        val gson = Gson()
        val type = object : TypeToken<List<Event>>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    @JvmStatic
    fun toNoteList(value: String): List<Event> {
        val gson = Gson()
        val type = object : TypeToken<List<Event>>() {}.type
        return gson.fromJson(value, type)
    }

//    @TypeConverter
//    @JvmStatic
//    fun stringToIntList(data: String?): List<Int>? {
//        return data?.let {
//            it.split(",").map {
//                try {
//                    it.toInt()
//                } catch (ex: NumberFormatException) {
//                    Timber.e(ex, "Cannot convert $it to number")
//                    null
//                }
//            }
//        }?.filterNotNull()
//    }
//
//    @TypeConverter
//    @JvmStatic
//    fun intListToString(ints: List<Int>?): String? {
//        return ints?.joinToString(",")
//    }
//
//    @TypeConverter
//    @JvmStatic
//    fun stringToStringList(data: String?): List<String>? {
//        return data?.split(",")
//    }
//
//    @TypeConverter
//    @JvmStatic
//    fun stringListToString(ints: List<String>?): String? {
//        return ints?.joinToString(",")
//    }
}

