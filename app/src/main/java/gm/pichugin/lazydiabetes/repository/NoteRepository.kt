package gm.pichugin.lazydiabetes.repository

import androidx.lifecycle.LiveData
import gm.pichugin.lazydiabetes.AppExecutors
import gm.pichugin.lazydiabetes.db.NoteDao
import gm.pichugin.lazydiabetes.vo.Note
import java.util.*
import javax.inject.Inject

class NoteRepository @Inject constructor(
    private val appExecutors: AppExecutors,
    private val dao: NoteDao
) {
    fun loadNote(noteId: Int): LiveData<Note> {
        return dao.loadNote(noteId)
    }

    fun loadNotes(date: Calendar): LiveData<List<Note>> {
        return dao.loadNotes(date)
    }

    fun insertNotes(notes: List<Note>) {
        appExecutors.diskIO().execute {
            dao.insertNotes(notes)
        }
    }

    fun updateNotes(notes: List<Note>) {
        appExecutors.diskIO().execute {
            notes.forEach {
                dao.updateNote(it)
            }
        }
    }

//    fun updateFood(productId: Int, favourite: Boolean) {
//        appExecutors.diskIO().execute {
//            dao.updateProduct(productId, favourite)
//        }
//    }
//
//    fun updateFood(food: Food) {
//        appExecutors.diskIO().execute {
//            dao.updateProduct(food)
//        }
//    }
}