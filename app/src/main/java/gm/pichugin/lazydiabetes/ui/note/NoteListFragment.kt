package gm.pichugin.lazydiabetes.ui.note

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import dagger.android.support.DaggerFragment
import gm.pichugin.lazydiabetes.AppExecutors
import gm.pichugin.lazydiabetes.R
import gm.pichugin.lazydiabetes.binding.FragmentDataBindingComponent
import gm.pichugin.lazydiabetes.databinding.FragmentNoteListBinding
import gm.pichugin.lazydiabetes.util.autoCleared
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class NoteListFragment : DaggerFragment() {
    @Inject
    lateinit var appExecutors: AppExecutors
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: NoteListViewModel by activityViewModels { viewModelFactory }

//    private val params by navArgs<FoodListFragmentArgs>()
    private var binding by autoCleared<FragmentNoteListBinding>()
    private var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    private var adapter by autoCleared<NoteListAdapter>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataBinding = DataBindingUtil.inflate<FragmentNoteListBinding>(
            inflater,
            R.layout.fragment_note_list,
            container,
            false,
            dataBindingComponent
        )

        binding = dataBinding
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        viewModel.prePopulateDb()
        viewModel.setDate(Calendar.getInstance())

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.notes = viewModel.notes

        val rvAdapter = NoteListAdapter(dataBindingComponent, appExecutors, viewModel) {}

        val recyclerView = binding.noteList
//        (recyclerView.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        recyclerView.adapter = rvAdapter
        this.adapter = rvAdapter

        adapter.submitList(viewModel.prePopulateDb())

//        viewModel.notes.observe(viewLifecycleOwner, Observer {
//            adapter.submitList(it)
//        })
    }
}
