package gm.pichugin.lazydiabetes.ui.note

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import gm.pichugin.lazydiabetes.AppExecutors
import gm.pichugin.lazydiabetes.R
import gm.pichugin.lazydiabetes.databinding.ItemNoteBinding
import gm.pichugin.lazydiabetes.ui.common.DataBoundListAdapter
import gm.pichugin.lazydiabetes.vo.Note

class NoteListAdapter(
    private val dataBindingComponent: DataBindingComponent,
    appExecutors: AppExecutors,
    private val viewModel: NoteListViewModel,
    private val itemClickCallback: ((Note) -> Unit)?
) : DataBoundListAdapter<Note, ItemNoteBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<Note>() {
        override fun areItemsTheSame(oldItem: Note, newItem: Note): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Note, newItem: Note): Boolean {
            return oldItem == newItem
        }
    }
) {
    override fun createBinding(parent: ViewGroup): ItemNoteBinding {
        val binding = DataBindingUtil.inflate<ItemNoteBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_note,
            parent,
            false,
            dataBindingComponent
        )

//        binding.productCard.setOnClickListener {
//            binding.food?.let {
//                itemClickCallback?.invoke(it)
//            }
//        }

        return binding
    }

    override fun bind(binding: ItemNoteBinding, item: Note) {
        binding.note = item
        binding.viewModel = viewModel
    }
}