package gm.pichugin.lazydiabetes.ui.note

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import gm.pichugin.lazydiabetes.repository.NoteRepository
import gm.pichugin.lazydiabetes.vo.Event
import gm.pichugin.lazydiabetes.vo.EventType
import gm.pichugin.lazydiabetes.vo.Note
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class NoteListViewModel @Inject constructor(
    private val repository: NoteRepository
) : ViewModel() {
    private val _date = MutableLiveData<Calendar>()
    val date: LiveData<Calendar>
        get() = _date

    var notes: LiveData<List<Note>> = Transformations
        .switchMap(_date) { date ->
            repository.loadNotes(date)
        }


    fun setDate(date: Calendar) {
        if (_date.value != date) {
            _date.value = date
        }
    }

    fun prePopulateDb(): List<Note> {
        val calendar = Calendar.getInstance()

        val mmolPerL = "ммоль/л"
        val units = "ед."
        val xe = "ХЕ"

        val event0_1 = Event(0, EventType.BASAL, "14", units)
        val event0_2 = Event(0, EventType.FOOD, "1", xe)
        val event0_3 = Event(0, EventType.GLUCOSE, "4.3", mmolPerL)
        val el1 = listOf(event0_1, event0_2, event0_3)

        val event1_1 = Event(1, EventType.BOLUS, "10", units)
        val event1_2 = Event(1, EventType.FOOD, "10", xe)
        val event1_3 = Event(1, EventType.GLUCOSE, "5.8", mmolPerL)
        val el2 = listOf(event1_1, event1_2, event1_3)

        val event2_1 = Event(2, EventType.GLUCOSE, "5.8", mmolPerL)
        val el3 = listOf(event2_1)

        val list = listOf(
            Note(0, calendar, el1),
            Note(1, calendar, el2),
            Note(2, calendar, el3)
        )

        repository.updateNotes(list)

        return list
    }

    fun getTimeString(calendar: Calendar): String {
        val simpleDateFormat = SimpleDateFormat("hh:mm")
        return simpleDateFormat.format(calendar.time)
    }
//    fun switchFavourite(food: Food) {
//        repository.updateFood(food.id, food.favourite.not())
//    }
}