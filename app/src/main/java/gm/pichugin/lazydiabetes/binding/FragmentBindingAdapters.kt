package gm.pichugin.lazydiabetes.binding

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.fragment.app.Fragment
import javax.inject.Inject

/**
 * Binding adapters that work with a fragment instance.
 */
class FragmentBindingAdapters @Inject constructor(val fragment: Fragment) {
//    @BindingAdapter("imageUrl")
//    fun bindImage(imageView: ImageView, url: String?) {
//        Glide
//            .with(fragment)
//            .load(url)
//            .centerCrop()
////            .placeholder(R.drawable.ic_element_placeholder)
////            .error(R.drawable.ic_baseline_image_24px)
//            .into(imageView)
//
//    }

    @BindingAdapter("android:text")
    fun setFloat(view: TextView, value: Float) {
        view.text = value.toString()
    }

    @InverseBindingAdapter(attribute = "android:text")
    fun getFloat(view: TextView): Float {
        val num = view.text.toString()

        return try {
            num.toFloat()
        } catch (e: NumberFormatException) {
            0f
        }
    }
}