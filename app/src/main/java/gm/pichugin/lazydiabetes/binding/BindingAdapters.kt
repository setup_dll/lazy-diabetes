package gm.pichugin.lazydiabetes.binding

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import gm.pichugin.lazydiabetes.BR


object BindingAdapters {
    @JvmStatic
    @BindingAdapter("visibleGone")
    fun showHide(view: View, show: Boolean) {
        view.visibility = if (show) View.VISIBLE else View.GONE
    }

    @JvmStatic
    @BindingAdapter("entries", "layout")
    fun <T> setEntries(
        viewGroup: ViewGroup,
        entries: List<T>?, layoutId: Int
    ) {
        viewGroup.removeAllViews()
        if (entries != null) {
            val inflater =
                viewGroup.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            for (entry in entries) {
                val binding = DataBindingUtil
                    .inflate<ViewDataBinding>(inflater, layoutId, viewGroup, true)
                binding.setVariable(BR.entry, entry)
            }
        }
    }
}
//
//@BindingAdapter("goneUnless")
//fun goneUnless(view: View, visibility: MutableLiveData<Boolean>?) {
//    val parentActivity: AppCompatActivity? = view.getParentActivity()
//    if (parentActivity != null && visibility != null) {
//        visibility.observe(parentActivity, Observer { value ->
//            val visible = value ?: true
//            view.visibility = if (visible) View.VISIBLE else View.GONE
//        })
//    }
//}
//
//@BindingAdapter("mutableVisibility")
//fun setMutableVisibility(view: View, visibility: MutableLiveData<Int>?) {
//    val parentActivity: AppCompatActivity? = view.getParentActivity()
//    if (parentActivity != null && visibility != null) {
//        visibility.observe(parentActivity, Observer { value -> view.visibility = value ?: View.VISIBLE })
//    }
//}
//
//@BindingAdapter("mutableText")
//fun setMutableText(view: TextView, text: MutableLiveData<String>?) {
//    val parentActivity: AppCompatActivity? = view.getParentActivity()
//    if (parentActivity != null && text != null) {
//        text.observe(parentActivity, Observer { value -> view.text = value ?: "" })
//    }
//}
