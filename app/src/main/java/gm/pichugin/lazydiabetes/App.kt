package gm.pichugin.lazydiabetes

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import gm.pichugin.lazydiabetes.di.DaggerAppComponent

class App : DaggerApplication() {
    private val applicationInjector = DaggerAppComponent.builder()
        .application(this)
        .build()

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = applicationInjector
}