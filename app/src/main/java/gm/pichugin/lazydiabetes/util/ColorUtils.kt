package gm.pichugin.lazydiabetes.util

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import gm.pichugin.lazydiabetes.R

class ColorUtils(context: Context) {
    // Gl: https://www.healthline.com/nutrition/low-glycemic-diet#section3

    val mediumGlycemicIndex = 56
    val highGlycemicIndex = 70

    val colorLow = ContextCompat.getColor(context, R.color.colorLow)
    val colorMedium = ContextCompat.getColor(context, R.color.colorMedium)
    val colorHigh = ContextCompat.getColor(context, R.color.colorHigh)

    val favStar = ContextCompat.getColor(context, R.color.colorFavStar)
    val favStarSelected = ContextCompat.getColor(context, R.color.colorFavStarSelected)

    fun getGiColor(index: Int): Int {
        val startColor: Int
        val endColor: Int
        var ratio = 1f

        if (index >= mediumGlycemicIndex) {
            if (index >= highGlycemicIndex) {
                startColor = colorHigh
                endColor = colorHigh
            } else {
                startColor = colorMedium
                endColor = colorHigh
                ratio = index / highGlycemicIndex.toFloat()
            }
        } else {
            startColor = colorLow
            endColor = colorMedium
            ratio = index / mediumGlycemicIndex.toFloat()
        }

        return ColorUtils.blendARGB(startColor, endColor, ratio)
    }

    fun getRatingColor(favourite: Boolean): Int {
        return if (!favourite) favStar else favStarSelected
    }
}
