package gm.pichugin.lazydiabetes.db

import androidx.lifecycle.LiveData
import androidx.room.*
import gm.pichugin.lazydiabetes.vo.Note
import java.util.*

@Dao
interface NoteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNote(note: Note)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNotes(notes: List<Note>)

    @Update
    fun updateNote(note: Note)

//    @Query("UPDATE Note SET favourite = :favourite WHERE id = :foodId")
//    fun updateProduct(foodId: Int, favourite: Boolean)

    @Query("SELECT * FROM Note WHERE calendar = :calendar")
    fun loadNotes(calendar: Calendar): LiveData<List<Note>>

    @Query("SELECT * FROM Note WHERE id = :foodId LIMIT 1")
    fun loadNote(foodId: Int): LiveData<Note>
}