package gm.pichugin.lazydiabetes.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import gm.pichugin.lazydiabetes.api.DbTypeConverters
import gm.pichugin.lazydiabetes.vo.Note

@Database(
    entities = [
        Note::class
    ], version = 1, exportSchema = false
)
@TypeConverters(DbTypeConverters::class)
abstract class Database : RoomDatabase() {
    abstract fun noteDao(): NoteDao
}